import express, { Request, Response, Router } from "express";

const indexRouter: Router = Router();


indexRouter.get('/', (req: Request, res: Response) => {
    res.send({message: 'Hello World!'})
  })

  export default indexRouter;
  