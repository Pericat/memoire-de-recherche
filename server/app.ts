
import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";
import indexRouter from "./routes/index";

dotenv.config();

export const app = express();
const port = process.env.Port || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }))


app.use("/", indexRouter);


// catch 404 and forward to error handler
app.use(function(req: Request, res: Response, next) {
    res.status(404) 
  });

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})