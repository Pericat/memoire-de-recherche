import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { RequestHttpComponent } from './core/request-http/request-http.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RequestHttpComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'client';
}
