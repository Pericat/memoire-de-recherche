import { Component } from '@angular/core';

@Component({
  selector: 'app-request-http',
  standalone: true,
  imports: [],
  templateUrl: './request-http.component.html',
  styleUrl: './request-http.component.scss'
})
export class RequestHttpComponent {

  callServer(){
    fetch('http://localhost:3000/', {
            
            method: 'GET',
        })
        .then((response) => response.json())
  .then((json) => console.log(json.message));

  }

}
